#!/bin/bash

# Jorge Ernesto Guevara Cuenca
# Order books downloaded from https://www.humblebundle.com
# Create a directory per book and move all formats of the book to this directory.

set -e

while getopts ":x" opt; do
	case $opt in
		x)
			set -x
			;;
		\?)
			echo "Usage: $0 [-x] /path/to/books"
			exit 1
			;;
	esac
done
shift $((OPTIND -1))

BOOKS_PATH="${1:?"Error. No path to books provided"}"

# Get the list of books in PDF format because at least this format is provided by humblebundle
BOOKS=$(find "$BOOKS_PATH" -maxdepth 1 -type f -iname '*.pdf')

for book in $BOOKS; do
	BASE_NAME=${book##*/}
	BOOK_NAME=${BASE_NAME%%.pdf}
	BOOK_DIR="${BOOKS_PATH}/${BOOK_NAME}"
	test -d "$BOOK_DIR" || mkdir "$BOOK_DIR"
	BOOK_FORMATS=$(ls "$BOOK_DIR"*.*)
	if [[ -n $BOOK_FORMATS ]]; then
		for book_format in  $BOOK_FORMATS; do
			mv "$book_format" "$BOOK_DIR"
		done
	fi
done
