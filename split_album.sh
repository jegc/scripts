#!/bin/bash

# Jorge Ernesto Guevara Cuenca
# Split a flac, wabpack or ape file in multiple tracks using a .cue file
# References:
# https://danilodellaquila.com/en/blog/how-to-split-an-audio-flac-file-using-ubuntu-linux
# https://wiki.archlinux.org/index.php/CUE_Splitting
# http://technoergonomics.com/articles/2011/12/converting-audio-file-formats-ape-cue-flac-linux

set -e

#Set -x before option to debug output
while getopts ":x" opt; do
    case $opt in
        x) # Print a trace of simple commands
            set -x
            ;;
        \?)
            echo "Usage: $0 [-x] file.cue file.[ape|flac|wv]"
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

CUE_FILE="${1:?"Error. Missing cue file"}"
MULTIMEDIA_FILE="${2:?"Error. Missing flac, wv or ape file"}"

FILE_NAME=${MULTIMEDIA_FILE%*.*}
FORMAT=${MULTIMEDIA_FILE##*.}

case $FORMAT in
	"flac")
		cuebreakpoints "$CUE_FILE" | shnsplit -o flac "$MULTIMEDIA_FILE"
		cuetag "$CUE_FILE" split-track??.flac
		;;
	"ape"|"wv")
		ffmpeg -i "$MULTIMEDIA_FILE" "${FILE_NAME}.wav"
		bchunk -w "${FILE_NAME}.wav" "$CUE_FILE" "$FILE_NAME"
		flac --best "$FILE_NAME"??.wav
		rm ./*.wav
		cuetag "$CUE_FILE" ./*.flac
		;;
	*)
		echo "Error: Unknown format"
		exit 1
		;;
esac
